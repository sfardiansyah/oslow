# -*- coding: utf-8 -*-
import os
import scrapy
import re
import datetime
import psycopg2
from operator import methodcaller


class CilacapSpider(scrapy.Spider):
    name = 'cilacap'
    allowed_domains = ['maritim.bmkg.go.id']
    start_urls = [
        'http://maritim.bmkg.go.id/stasiun_maritim/wilayah_perairan?stasiun=NInc8KV9LSiqJToZNuabm-0TJcdSp555knEJAGGrl5E']

    def parse(self, response):
        titles = response.css('div.head_cuca::text')[2:].extract()
        items = response.css('div.kan_kon_mar::text').extract()

        hi_waves = map(lambda x: re.match(
            r"\r\n\t+(\d+(\.\d+)?)\t+ - (\d+(\.\d+)?)", x).group(3), items[2::3])
        low_waves = map(lambda x: re.match(
            r"\r\n\t+(\d+(\.\d+)?)\t+ - (\d+(\.\d+)?)", x).group(1), items[2::3])

        hi_winds = map(lambda x: re.match(
            r"\r\n\t+(\w+) - (\w+)\,\r\n\t+(\d+)\t+ - (\d+)", x).group(4), items[1::3])
        low_winds = map(lambda x: re.match(
            r"\r\n\t+(\w+) - (\w+)\,\r\n\t+(\d+)\t+ - (\d+)", x).group(3), items[1::3])

        left_wind_dir = map(lambda x: re.match(
            r"\r\n\t+(\w+) - (\w+)\,\r\n\t+(\d+)\t+ - (\d+)", x).group(1), items[1::3])
        right_wind_dir = map(lambda x: re.match(
            r"\r\n\t+(\w+) - (\w+)\,\r\n\t+(\d+)\t+ - (\d+)", x).group(2), items[1::3])

        sql = """INSERT INTO maritim.stations(name, low_waves, hi_waves, rain, left_wind_dir, right_wind_dir, low_wind, hi_wind)
             VALUES(%s, %s, %s, %s, %s, %s, %s, %s);"""

        try:
            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(os.environ['HEROKU_POSTGRESQL_NAVY_URL'])

            # create a cursor
            cur = conn.cursor()

            # execute a statement
            for i in range(len(items)/3):
                cur.execute(sql, (titles[i], float(low_waves[i]), float(
                    hi_waves[i]), items[::3][i], left_wind_dir[i], right_wind_dir[i], int(low_winds[i]), int(hi_winds[i])))

            # commit the changes to the database
            conn.commit()

            # close the communication with the PostgreSQL
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
                print('Database connection closed.')
